package com.abi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abi.entity.Person;
import com.abi.service.PersonService;

@RestController
public class PersonController {
	
	@Autowired
	private PersonService service;
	
	@GetMapping("/person")
	private List<Person> getAll(){
		return service.getAll();
	}
	
	@GetMapping("/person/{id}")
	private Person get(@PathVariable("id") int id){
		return service.getById(id);
	}
	
	@DeleteMapping("/person/{id}")
	private void delete(@PathVariable("id") int id){
		service.delete(id);
	}
	
	@PostMapping("/person")
	private int save(@RequestBody Person person) {
		service.saveOrUpdate(person);
		return person.getId();
	}
	
}
