package com.abi.repository;

import org.springframework.data.repository.CrudRepository;

import com.abi.entity.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {
}