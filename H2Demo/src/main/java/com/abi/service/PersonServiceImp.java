package com.abi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abi.entity.Person;
import com.abi.repository.PersonRepository;

@Service
public class PersonServiceImp implements PersonService {

	@Autowired
	private PersonRepository repository;
	
	@Override
	public List<Person> getAll() {
		List<Person> persons = new ArrayList<>();
		repository.findAll().forEach(person -> persons.add(person));
		return persons;
	}

	@Override
	public Person getById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Person person) {
		repository.save(person);
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);
	}

}