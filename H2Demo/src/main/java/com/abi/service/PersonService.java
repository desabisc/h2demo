package com.abi.service;

import java.util.List;

import com.abi.entity.Person;

public interface PersonService {
	
	public List<Person> getAll();
	
	public Person getById(int id);
	
	public void saveOrUpdate(Person person);
	
	public void delete(int id);
	
}